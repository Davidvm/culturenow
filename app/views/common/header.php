<header>
    <div class="logo-box">
        <a class="logo" href="/"><img alt="" src="/culturenow/app/assets/images/logo.png"></a>
    </div>

    <a class="elemadded responsive-link" href="#">Menu</a>

    <div class="menu-box">
        <ul class="menu">
            <li><a href="home"><span>HOME</span></a></li>
            <li><a href="cine"><span>CINE</span></a></li>
            <li><a href="teatro"><span>TEATRO</span></a></li>
            <li><a href="musica"><span>MÚSICA</span></a></li>
            <li><a href="eventos"><span>EVENTOS</span></a></li>
            <li><a href="videojuegos"><span>VIDEOJUEGOS</span></a></li>
            <li><a href="contacto"><span>CONTACTO</span></a></li>
            <li>
                <?php if (empty($_SESSION)){ ?>
                <a href="login"><span>ACCESO</span></a></li>
            <?php } else { ?>
                <a href="main"><span>MI PANEL</span></a></li>
            <?php } ?>
        </ul>
    </div>

    <div class="social-box">
        <ul class="social-icons">
            <li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
            <li><a href="#" class="google"><i class="fa fa-google-plus"></i></a></li>
            <li><a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
            <li><a href="#" class="pinterest"><i class="fa fa-pinterest"></i></a></li>
            <li><a href="#" class="youtube"><i class="fa fa-youtube"></i></a></li>
            <li><a href="#" class="github"><i class="fa fa-github"></i></a></li>
        </ul>
        <p class="copyright">&#169; 2014 Kappe, All Rights Reserved</p>
    </div>
</header>