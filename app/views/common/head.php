
<meta charset="utf-8">

<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic' rel='stylesheet' type='text/css'>

<link rel="stylesheet" type="text/css" href="/culturenow/app/assets/css/bootstrap.css" media="screen">
<link rel="stylesheet" type="text/css" href="/culturenow/app/assets/css/magnific-popup.css" media="screen">
<link rel="stylesheet" type="text/css" href="/culturenow/app/assets/css/font-awesome.css" media="screen">
<link rel="stylesheet" type="text/css" href="/culturenow/app/assets/css/flexslider.css" media="screen">
<link rel="stylesheet" type="text/css" href="/culturenow/app/assets/css/style.css" media="screen">
<link rel="stylesheet" type="text/css" href="/culturenow/app/assets/css/responsive.css" media="screen">