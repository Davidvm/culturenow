<!doctype html>

<html lang="es" class="no-js">
<head>
    <title>CultureNow - Login</title>
    <?php include(ROOT . "/culturenow/app/views/common/head.php"); ?>
</head>
<body>
<!-- Container -->
<div id="container">
    <!-- Header
        ================================================== -->
    <?php include(ROOT . "/culturenow/app/views/common/header.php"); ?>
    <!-- End Header -->

    <!-- content
        ================================================== -->
    <div id="content">
        <div class="inner-content">
            <div class="contact-page">
                <div class="contact-box">
                    <form id="contact-form" method="POST" action="<?= FOLDER_PATH . '/login/signin' ?>">
                        <h1>Log in</h1>
                        <div class="float-input">
                            <input type="text" name="email" id="inputEmail" placeholder="Email address" required autofocus>
                            <span><i class="fa fa-user"></i></span>
                        </div>
                        <div class="float-input">
                            <input type="password" name="password" id="inputPassword" placeholder="Password" >
                            <span><i class="fa fa-pencil"></i></span>
                        </div>
                        <div class="col-md-6" style="margin-top: 30px;">
                           <p>¿Quieres registrarte? haz clic <a href="register">Registrarse</a></p>
                        </div>

                        <div class="col-md-6">
                            <button class="btn btn-lg btn-primary btn-block" type="submit">Acceder</button>
                            <div id="msg" class="message"><?php !empty($error_message) ? print($error_message) : '' ?></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- End content -->

</div>
<!-- End Container -->

<div class="preloader">
    <img alt="" src="images/preloader.gif">
</div>
<?php include(ROOT . "/culturenow/app/views/common/scripts.php"); ?>


</body>
</html>
