<!DOCTYPE html>
<html lang="es" class="no-js">
<head>
    <title>CultureNow - Perfíl</title>
    <?php include(ROOT . "/culturenow/app/views/common/head.php"); ?>
</head>
<body>

<!-- Container -->
<div id="container">
    <!-- Header
        ================================================== -->
    <?php include(ROOT . "/culturenow/app/views/common/header.php"); ?>
    <!-- End Header -->

    <!-- content
        ================================================== -->
    <div id="content">
        <div class="inner-content">
            <div class="contact-page">

                <div class="contact-box">
                    <form id="contact-form" method="POST" action="<?= FOLDER_PATH . '/profile/updateUser' ?>">
                        <a href="main" class="pull-right">Volver</a>
                        <h1>Perfil de usuario</h1>

                        <div class="text-fields">
                            <div class="float-input">
                                <input name="nombre" id="nombre" type="text" value="<?php echo $user['nombre']; ?>" placeholder="Nombre">
                                <span><i class="fa fa-user"></i></span>
                            </div>
                            <div class="float-input">
                                <input name="apellidos" id="apellidos" value="<?php echo $user['apellidos']; ?>" type="text" placeholder="Apellidos">
                                <span><i class="fa fa-user"></i></span>
                            </div>
                            <div class="float-input">
                                <input name="email" id="email" value="<?php echo $user['email']; ?>" type="text" placeholder="e-mail">
                                <span><i class="fa fa-envelope-o"></i></span>
                            </div>
                        </div>
                        <div class="text-fields">
                            <div class="float-input">
                                <input name="password" type="password" placeholder="Contraseña">
                                <span><i class="fa fa-pencil"></i></span>
                            </div>
                            <div class="float-input">
                                <input name="repassword" type="password"
                                       placeholder="Repite la contraseña">
                                <span><i class="fa fa-pencil"></i></span>
                            </div>
                            <div class="float-input">
                                <input name="fechaNacimiento" value="<?php echo $user['fechaNacimiento']; ?>" id="fechaNacimiento" type="date"
                                       placeholder="fechaNacimiento">
                                <span><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>

                        <button class="btn btn-lg btn-primary btn-block" type="submit">Modificar</button>
                        <div id="msg" class="message"><?php !empty($error_message) ? print($error_message) : '' ?></div>

                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- End content -->

    <!-- End content -->

</div>
<!-- End Container -->

<div class="preloader">
    <img alt="" src="images/preloader.gif">
</div>
<?php include(ROOT . "/culturenow/app/views/common/scripts.php"); ?>
</body>
</html>