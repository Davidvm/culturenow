<!DOCTYPE html>
<html lang="es" class="no-js">
<head>
    <title>CultureNow - Perfíl</title>
    <?php include(ROOT . "/culturenow/app/views/common/head.php"); ?>
</head>
<body>

<!-- Container -->
<div id="container">
    <!-- Header
        ================================================== -->
    <?php include(ROOT . "/culturenow/app/views/common/header.php"); ?>
    <!-- End Header -->

    <!-- content
        ================================================== -->
    <div id="content">
        <div class="inner-content">
            <div class="about-page">

                <div class="about-box">
                    <div class="about-content">
                        <div class="about-section">
                            <img alt="" src="/culturenow/app/assets/upload/about.jpg">
                            <h1>Bienvenido <?php echo $_SESSION['nombre']; ?></h1>
                            <p>En esta pantalla podras gestionar tus RSS. Puedes seleccionar
                                uno de los que te ofrecemos nosotros, buscar uno, agregar eliminar los tuyos
                                propios.</p>
                            <p>Si quieres modificar tus datos, puedes hacerlo en tu <a href="profile">Perfíl</a></p>
                        </div>
                        <div class="about-section last-section">
                            <h1>Tus RSS</h1>
                            <ul>
                                <?php foreach($rss as $item){?>
                                <li>
                                    <a href="<?= FOLDER_PATH . '/main/moveToTrash/'. $item['id_rss']; ?>"><i class="fa fa-trash-o"></i></a>
                                    <a href="<?php echo $item['link'];?>"><?php echo $item['titulo'];?> - <span><?php echo $item['link'];?></span></a>
                                 </li>
                                <? } ?>
                            </ul>
                            <form class="comment-form" style="margin: 30px 0;" method="POST"
                                  action="<?= FOLDER_PATH . '/main/SaveNewRss' ?>">
                                <h1>Agrega un nuevo RSS</h1>
                                <div class="text-fields">
                                    <div class="float-input">
                                        <input name="url" type="text" placeholder="RSS">
                                        <span><i class="fa fa-link"></i></span>
                                    </div>
                                </div>
                                <div class="text-fields">
                                    <div class="float-input">
                                        <input name="titulo" type="text" placeholder="Nombre">
                                        <span><i class="fa fa-folder-open"></i></span>
                                    </div>
                                </div>

                                <button class="btn btn-lg btn-primary btn-block" type="submit">Agregar</button>

                            </form>

                        </div>
                    </div>
                    <div class="sidebar">

                        <?php

                        $url = "http://makitweb.com/feed/";
                        if (isset($rss)) {
                            if ($rss != '') {
                                $url = $rss[0]['link'];
                            }
                        }

                        $invalidurl = false;
                        if(@simplexml_load_file($url)){
                            $feeds = simplexml_load_file($url);
                        }else{
                            $invalidurl = true;
                            echo "<h2>Invalid RSS feed URL.</h2>";
                        }

                        $i = 0;
                        if (!empty($feeds)) {


                            $site = $feeds->channel->title;
                            $sitelink = $feeds->channel->link;

                            echo "<h1>" . $site . "</h1>";
                            foreach ($feeds->channel->item as $item) {

                                $title = $item->title;
                                $link = $item->link;
                                $description = $item->description;
                                $postDate = $item->pubDate;
                                $pubDate = date('D, d M Y', strtotime($postDate));


                                if ($i >= 5) break;
                                ?>
                                <div class="post">
                                    <div class="post-head">
                                        <h2><a class="feed_title" href="<?php echo $link; ?>"><?php echo $title; ?></a>
                                        </h2>
                                        <span><?php echo $pubDate; ?></span>
                                    </div>
                                    <div class="post-content">
                                        <?php echo implode(' ', array_slice(explode(' ', $description), 0, 20)) . "..."; ?>
                                        <a href="<?php echo $link; ?>">Leer más</a>
                                    </div>
                                </div>

                                <?php
                                $i++;
                            }
                        } else {
                            if (!$invalidurl) {
                                echo "<h2>No item found</h2>";
                            }
                        }
                        ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End content -->

</div>
<!-- End Container -->

<div class="preloader">
    <img alt="" src="images/preloader.gif">
</div>
<?php include(ROOT . "/culturenow/app/views/common/scripts.php"); ?>
</body>
</html>