<!DOCTYPE html>
<html lang="es" class="no-js">
<head>
	<title>CultureNow - Home</title>
	<?php include(ROOT . "/culturenow/app/views/common/head.php"); ?>
</head>
<body>

	<!-- Container -->
	<div id="container">
		<!-- Header
		    ================================================== -->
			<?php include(ROOT . "/culturenow/app/views/common/header.php"); ?>
		<!-- End Header -->

		<!-- content 
			================================================== -->
		<div id="content">
			<div class="inner-content">
				<div class="about-page">

					<div class="about-box">
						<div class="about-content">
                            <h1>Error 404</h1>
                            <h2>Oops! Página no encontrada</h2>
                            <a href="<?php echo $path_inicio ?>">Ir a inicio</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- End content -->

	</div>
	<!-- End Container -->

	<div class="preloader">
		<img alt="" src="images/preloader.gif">
	</div>
	<?php include(ROOT . "/culturenow/app/views/common/scripts.php"); ?>
</body>
</html>