<!DOCTYPE html>
<html lang="es" class="no-js">
<head>
	<title>CultureNow - Teatro</title>
	<?php include(ROOT . "/culturenow/app/views/common/head.php"); ?>
</head>
<body>
	<!-- Container -->
	<div id="container">
		<!-- Header
		    ================================================== -->
			<?php include(ROOT . "/culturenow/app/views/common/header.php"); ?>
		<!-- End Header -->

		<!-- content 
			================================================== -->
		<div id="content">
			<div class="inner-content">
				<div class="portfolio-page">
					<div class="portfolio-box">

						<div class="project-post web-design illustration">
							<img alt="" src="app/assets/upload/image1.jpg">
							<div class="hover-box">
								<div class="project-title">
									<h2>Cool App Design</h2>
									<span>development, mobile</span>
									<div><a href="single-project.html"><i class="fa fa-arrow-right"></i></a></div>
								</div>
							</div>
						</div>

						<div class="project-post photography">
							<img alt="" src="app/assets/upload/image2.jpg">
							<div class="hover-box">
								<div class="project-title">
									<h2>Cool App Design</h2>
									<span>development, mobile</span>
									<div><a href="single-project.html"><i class="fa fa-arrow-right"></i></a></div>
								</div>
							</div>
						</div>

						<div class="project-post web-design logo">
							<img alt="" src="app/assets/upload/image3.jpg">
							<div class="hover-box">
								<div class="project-title">
									<h2>Cool App Design</h2>
									<span>development, mobile</span>
									<div><a href="single-project.html"><i class="fa fa-arrow-right"></i></a></div>
								</div>
							</div>
						</div>

						<div class="project-post photography illustration">
							<img alt="" src="app/assets/upload/image4.jpg">
							<div class="hover-box">
								<div class="project-title">
									<h2>Cool App Design</h2>
									<span>development, mobile</span>
									<div><a href="single-project.html"><i class="fa fa-arrow-right"></i></a></div>
								</div>
							</div>
						</div>

						<div class="project-post nature logo">
							<img alt="" src="app/assets/upload/image5.jpg">
							<div class="hover-box">
								<div class="project-title">
									<h2>Cool App Design</h2>
									<span>development, mobile</span>
									<div><a href="single-project.html"><i class="fa fa-arrow-right"></i></a></div>
								</div>
							</div>
						</div>

						<div class="project-post web-design illustration">
							<img alt="" src="app/assets/upload/image6.jpg">
							<div class="hover-box">
								<div class="project-title">
									<h2>Cool App Design</h2>
									<span>development, mobile</span>
									<div><a href="single-project.html"><i class="fa fa-arrow-right"></i></a></div>
								</div>
							</div>
						</div>

						<div class="project-post photography">
							<img alt="" src="app/assets/upload/image7.jpg">
							<div class="hover-box">
								<div class="project-title">
									<h2>Cool App Design</h2>
									<span>development, mobile</span>
									<div><a href="single-project.html"><i class="fa fa-arrow-right"></i></a></div>
								</div>
							</div>
						</div>

						<div class="project-post nature illustration">
							<img alt="" src="app/assets/upload/image8.jpg">
							<div class="hover-box">
								<div class="project-title">
									<h2>Cool App Design</h2>
									<span>development, mobile</span>
									<div><a href="single-project.html"><i class="fa fa-arrow-right"></i></a></div>
								</div>
							</div>
						</div>

						<div class="project-post logo">
							<img alt="" src="app/assets/upload/image9.jpg">
							<div class="hover-box">
								<div class="project-title">
									<h2>Cool App Design</h2>
									<span>development, mobile</span>
									<div><a href="single-project.html"><i class="fa fa-arrow-right"></i></a></div>
								</div>
							</div>
						</div>

						<div class="project-post web-design nature">
							<img alt="" src="app/assets/upload/image10.jpg">
							<div class="hover-box">
								<div class="project-title">
									<h2>Cool App Design</h2>
									<span>development, mobile</span>
									<div><a href="single-project.html"><i class="fa fa-arrow-right"></i></a></div>
								</div>
							</div>
						</div>

						<div class="project-post photography illustration">
							<img alt="" src="app/assets/upload/image11.jpg">
							<div class="hover-box">
								<div class="project-title">
									<h2>Cool App Design</h2>
									<span>development, mobile</span>
									<div><a href="single-project.html"><i class="fa fa-arrow-right"></i></a></div>
								</div>
							</div>
						</div>

						<div class="project-post nature logo">
							<img alt="" src="app/assets/upload/image12.jpg">
							<div class="hover-box">
								<div class="project-title">
									<h2>Cool App Design</h2>
									<span>development, mobile</span>
									<div><a href="single-project.html"><i class="fa fa-arrow-right"></i></a></div>
								</div>
							</div>
						</div>

						<div class="project-post web-design illustration">
							<img alt="" src="app/assets/upload/image1.jpg">
							<div class="hover-box">
								<div class="project-title">
									<h2>Cool App Design</h2>
									<span>development, mobile</span>
									<div><a href="single-project.html"><i class="fa fa-arrow-right"></i></a></div>
								</div>
							</div>
						</div>

						<div class="project-post photography nature">
							<img alt="" src="app/assets/upload/image4.jpg">
							<div class="hover-box">
								<div class="project-title">
									<h2>Cool App Design</h2>
									<span>development, mobile</span>
									<div><a href="single-project.html"><i class="fa fa-arrow-right"></i></a></div>
								</div>
							</div>
						</div>

						<div class="project-post logo illustration">
							<img alt="" src="app/assets/upload/image5.jpg">
							<div class="hover-box">
								<div class="project-title">
									<h2>Cool App Design</h2>
									<span>development, mobile</span>
									<div><a href="single-project.html"><i class="fa fa-arrow-right"></i></a></div>
								</div>
							</div>
						</div>

						<div class="project-post photography logo">
							<img alt="" src="app/assets/upload/image2.jpg">
							<div class="hover-box">
								<div class="project-title">
									<h2>Cool App Design</h2>
									<span>development, mobile</span>
									<div><a href="single-project.html"><i class="fa fa-arrow-right"></i></a></div>
								</div>
							</div>
						</div>

						<div class="project-post nature">
							<img alt="" src="app/assets/upload/image12.jpg">
							<div class="hover-box">
								<div class="project-title">
									<h2>Cool App Design</h2>
									<span>development, mobile</span>
									<div><a href="single-project.html"><i class="fa fa-arrow-right"></i></a></div>
								</div>
							</div>
						</div>

						<div class="project-post logo illustration">
							<img alt="" src="app/assets/upload/image9.jpg">
							<div class="hover-box">
								<div class="project-title">
									<h2>Cool App Design</h2>
									<span>development, mobile</span>
									<div><a href="single-project.html"><i class="fa fa-arrow-right"></i></a></div>
								</div>
							</div>
						</div>

						<div class="project-post photography nature">
							<img alt="" src="app/assets/upload/image11.jpg">
							<div class="hover-box">
								<div class="project-title">
									<h2>Cool App Design</h2>
									<span>development, mobile</span>
									<div><a href="single-project.html"><i class="fa fa-arrow-right"></i></a></div>
								</div>
							</div>
						</div>

						<div class="project-post nature logo">
							<img alt="" src="app/assets/upload/image6.jpg">
							<div class="hover-box">
								<div class="project-title">
									<h2>Cool App Design</h2>
									<span>development, mobile</span>
									<div><a href="single-project.html"><i class="fa fa-arrow-right"></i></a></div>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
		<!-- End content -->

	</div>
	<!-- End Container -->

	<div class="info-box">
		<a class="info-toggle" href="#"><i class="fa fa-info-circle"></i></a>
		<div class="info-content">
			<ul>
				<li><i class="fa fa-phone"></i>9930 1234 5679</li>
				<li><i class="fa fa-envelope"></i><a href="#">contact@domain.com</a></li>
				<li><i class="fa fa-home"></i>street address example</li>
			</ul>
		</div>
	</div>

	<div class="preloader">
		<img alt="" src="app/assets/images/preloader.gif">
	</div>
	<?php include(ROOT . "/culturenow/app/views/common/scripts.php"); ?>
</body>
</html>