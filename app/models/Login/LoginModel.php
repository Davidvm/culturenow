<?php
defined('BASEPATH') or exit('No se permite acceso directo');
/**
 * Login Model
 */
class LoginModel extends Database {

    protected $nombre;
    protected $contrasena;
    /**
     * Inicia conexión DB
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return mixed
     */
    public function getContrasena()
    {
        return $this->contrasena;
    }

    /**
     * @param mixed $contrasena
     */
    public function setContrasena($contrasena)
    {
        $this->contrasena = $contrasena;
    }


    public function signIn($email, $password) {

        try {
            $connection = Database::instance();
            $query = $connection->prepare("SELECT * FROM Usuario WHERE email=:email AND contrasena=:password");
            $query->bindParam(':email',$email,PDO::PARAM_STR);
            $query->bindParam(':password',$password,PDO::PARAM_STR);
            $query->execute();

            return $query->fetch();

        } catch (\PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
    }
}