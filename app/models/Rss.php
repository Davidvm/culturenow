<?php
defined('BASEPATH') or exit('No se permite acceso directo');

/**
 * RSS Model
 */
class Rss extends Database
{
    public $idRss;
    public $titulo;
    public $link;
    public $id_user;

    /**
     * Inicia conexión DB
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return mixed
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param mixed $link
     */
    public function setLink($link)
    {
        $this->link = $link;
    }


    public function insertRss(Rss $data)
    {
        $connection = Database::instance();

        try {
            $sql = "INSERT INTO Rss (titulo,link,id_user) 
		        VALUES (?, ?, ?)";

            $connection->prepare($sql)
                ->execute(
                    array(
                        $data->titulo,
                        $data->link,
                        $data->id_user,
                    )
                );
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function deleteRss($id) {
        $connection = Database::instance();
        try
        {
            $stm = $connection->prepare("DELETE FROM Rss WHERE id_rss = ?");
            $stm->execute(array($id));
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }


}