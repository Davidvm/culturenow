<?php

defined('BASEPATH') or exit('No se permite acceso directo');
 /**
 * Rol Model
 */


class Rol extends Database
{
    protected $idRol;
    protected $rol;

    /**
     * Inicia conexión DB
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return mixed
     */
    public function getIdRol()
    {
        return $this->idRol;
    }

    /**
     * @param mixed $idRol
     */
    public function setIdRol($idRol)
    {
        $this->idRol = $idRol;
    }

    /**
     * @return mixed
     */
    public function getRol()
    {
        return $this->rol;
    }

    /**
     * @param mixed $rol
     */
    public function setRol($rol)
    {
        $this->rol = $rol;
    }


}