<?php
defined('BASEPATH') or exit('No se permite acceso directo');

/**
 * Usuario Model
 */
class Usuario extends Database
{

    public $idUsuario;
    public $nombre;
    public $apellidos;
    public $email;
    public $contrasena;
    public $fechaNacimiento;
    public $fechaRegistro;
    public $idRol;


    /**
     * Inicia conexión DB
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return mixed
     */
    public function getIdUsuario()
    {
        return $this->idUsuario;
    }

    /**
     * @param mixed $idUsuario
     */
    public function setIdUsuario($idUsuario)
    {
        $this->idUsuario = $idUsuario;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return mixed
     */
    public function getApellidos()
    {
        return $this->apellidos;
    }

    /**
     * @param mixed $apellidos
     */
    public function setApellidos($apellidos)
    {
        $this->apellidos = $apellidos;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getContrasena()
    {
        return $this->contrasena;
    }

    /**
     * @param mixed $contrasena
     */
    public function setContrasena($contrasena)
    {
        $this->contrasena = $contrasena;
    }

    /**
     * @return mixed
     */
    public function getFechaNacimiento()
    {
        return $this->fechaNacimiento;
    }

    /**
     * @param mixed $fechaNacimiento
     */
    public function setFechaNacimiento($fechaNacimiento)
    {
        $this->fechaNacimiento = $fechaNacimiento;
    }

    /**
     * @return mixed
     */
    public function getFechaRegistro()
    {
        return $this->fechaRegistro;
    }

    /**
     * @param mixed $fechaRegistro
     */
    public function setFechaRegistro($fechaRegistro)
    {
        $this->fechaRegistro = $fechaRegistro;
    }

    /**
     * @return mixed
     */
    public function getIdRol()
    {
        return $this->idRol;
    }

    /**
     * @param mixed $idRol
     */
    public function setIdRol($idRol)
    {
        $this->idRol = $idRol;
    }

    public function getProfileUser($id) {
        $connection = Database::instance();
        try {
            $stm = $connection->prepare("SELECT * FROM Usuario WHERE idUsuario = ?");
            $stm->execute(array($id));
            return $stm->fetch();
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function updateUser($data) {
        $connection = Database::instance();
        try {
            $sql = "UPDATE Usuario SET 
						nombre              = ?, 
                        apellidos           = ?,
                        email               = ?,
                        contrasena          = ?,
                        fechaNacimiento     = ? 
				    WHERE idUsuario = ?";

            $connection->prepare($sql)
                ->execute(
                    array(
                        $data->nombre,
                        $data->apellidos,
                        $data->email,
                        $data->contrasena,
                        $data->fechaNacimiento,
                        $data->idUsuario
                    )
                );
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }


    public function getRssUser($id) {
        $connection = Database::instance();

        try {
            $sql = $connection->prepare("SELECT * FROM Rss WHERE id_user = ?");
            $sql->execute(array($id));
            return $sql->fetchAll();

        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

}