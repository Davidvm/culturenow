<?php
defined('BASEPATH') or exit('No se permite acceso directo');
/**
 * Articulo Model
 */
class Articulo extends Database {
    protected $idArticulo;
    protected $titulo;
    protected $autor;
    protected $link;
    protected $fecha;
    protected $articulo;
    protected $idPais;
    protected $idCategoria;

    /**
     * Inicia conexión DB
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return mixed
     */
    public function getIdArticulo()
    {
        return $this->idArticulo;
    }

    /**
     * @param mixed $idArticulo
     */
    public function setIdArticulo($idArticulo)
    {
        $this->idArticulo = $idArticulo;
    }

    /**
     * @return mixed
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * @param mixed $titulo
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;
    }

    /**
     * @return mixed
     */
    public function getAutor()
    {
        return $this->autor;
    }

    /**
     * @param mixed $autor
     */
    public function setAutor($autor)
    {
        $this->autor = $autor;
    }

    /**
     * @return mixed
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param mixed $link
     */
    public function setLink($link)
    {
        $this->link = $link;
    }

    /**
     * @return mixed
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * @param mixed $fecha
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    }

    /**
     * @return mixed
     */
    public function getArticulo()
    {
        return $this->articulo;
    }

    /**
     * @param mixed $articulo
     */
    public function setArticulo($articulo)
    {
        $this->articulo = $articulo;
    }

    /**
     * @return mixed
     */
    public function getIdPais()
    {
        return $this->idPais;
    }

    /**
     * @param mixed $idPais
     */
    public function setIdPais($idPais)
    {
        $this->idPais = $idPais;
    }

    /**
     * @return mixed
     */
    public function getIdCategoria()
    {
        return $this->idCategoria;
    }

    /**
     * @param mixed $idCategoria
     */
    public function setIdCategoria($idCategoria)
    {
        $this->idCategoria = $idCategoria;
    }

    public function insertRss(Articulo $data){
        $connection = Database::instance();

        try {
            $sql = "INSERT INTO Usuario (nombre,apellidos,email,contrasena,fechaNacimiento,fechaRegistro) 
		        VALUES (?, ?, ?, ?, ?, ?)";

            $connection->prepare($sql)
                ->execute(
                    array(
                        $data->nombre,
                        $data->apellidos,
                        $data->email,
                        $data->contrasena,
                        $data->fechaNacimiento,
                        date('Y-m-d')
                    )
                );
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }


}