<?php
defined('BASEPATH') or exit('No se permite acceso directo');
 /**
 * Pais Model
 */


class Pais extends Database
{
    protected $idPais;

    protected $pais;

    /**
     * Inicia conexión DB
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return mixed
     */
    public function getIdPais()
    {
        return $this->idPais;
    }

    /**
     * @param mixed $idPais
     */
    public function setIdPais($idPais)
    {
        $this->idPais = $idPais;
    }

    /**
     * @return mixed
     */
    public function getPais()
    {
        return $this->pais;
    }

    /**
     * @param mixed $pais
     */
    public function setPais($pais)
    {
        $this->pais = $pais;
    }


}