<?php
defined('BASEPATH') or exit('No se permite acceso directo');
require_once ROOT . FOLDER_PATH .'/app/models/Usuario.php';


class RegisterModel extends Database {

    /**
     * Inicia conexión DB
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function insertUser(Usuario $data){
        $connection = Database::instance();

        try {
            $sql = "INSERT INTO Usuario (nombre,apellidos,email,contrasena,fechaNacimiento,fechaRegistro) 
		        VALUES (?, ?, ?, ?, ?, ?)";

            $connection->prepare($sql)
                ->execute(
                    array(
                        $data->nombre,
                        $data->apellidos,
                        $data->email,
                        $data->contrasena,
                        $data->fechaNacimiento,
                        date('Y-m-d')
                    )
                );
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

}