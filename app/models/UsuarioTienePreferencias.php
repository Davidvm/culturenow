<?php
defined('BASEPATH') or exit('No se permite acceso directo');
/**
 * UsuarioTienePreferencias Model
 */

class UsuarioTienePreferencias extends Database
{
    protected $idPreferencias;
    protected $idUsuario;
    protected $idCategoria;
    protected $fechaCreacion;
    protected $idPais;


    /**
     * Inicia conexión DB
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return mixed
     */
    public function getIdPreferencias()
    {
        return $this->idPreferencias;
    }

    /**
     * @param mixed $idPreferencias
     */
    public function setIdPreferencias($idPreferencias)
    {
        $this->idPreferencias = $idPreferencias;
    }

    /**
     * @return mixed
     */
    public function getIdUsuario()
    {
        return $this->idUsuario;
    }

    /**
     * @param mixed $idUsuario
     */
    public function setIdUsuario($idUsuario)
    {
        $this->idUsuario = $idUsuario;
    }

    /**
     * @return mixed
     */
    public function getIdCategoria()
    {
        return $this->idCategoria;
    }

    /**
     * @param mixed $idCategoria
     */
    public function setIdCategoria($idCategoria)
    {
        $this->idCategoria = $idCategoria;
    }

    /**
     * @return mixed
     */
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * @param mixed $fechaCreacion
     */
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;
    }

    /**
     * @return mixed
     */
    public function getIdPais()
    {
        return $this->idPais;
    }

    /**
     * @param mixed $idPais
     */
    public function setIdPais($idPais)
    {
        $this->idPais = $idPais;
    }


}