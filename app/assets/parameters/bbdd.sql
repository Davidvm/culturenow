-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Temps de generació: 27-04-2019 a les 21:08:53
-- Versió del servidor: 5.7.25-0ubuntu0.16.04.2
-- Versió de PHP: 7.1.27-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de dades: `allYour`
--

-- --------------------------------------------------------

--
-- Estructura de la taula `Articulo`
--

CREATE TABLE `Articulo` (
  `idArticulo` int(11) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `autor` varchar(40) DEFAULT NULL,
  `link` varchar(255) NOT NULL,
  `fecha` date NOT NULL,
  `articulo` longtext NOT NULL,
  `idPais` int(11) NOT NULL,
  `idCategoria` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de la taula `Categoria`
--

CREATE TABLE `Categoria` (
  `idCategoria` int(11) NOT NULL,
  `categoria` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Bolcant dades de la taula `Categoria`
--

INSERT INTO `Categoria` (`idCategoria`, `categoria`) VALUES
(1, 'Cine'),
(2, 'Música'),
(3, 'Eventos'),
(4, 'Videojuegos');

-- --------------------------------------------------------

--
-- Estructura de la taula `Pais`
--

CREATE TABLE `Pais` (
  `idPais` int(11) NOT NULL,
  `pais` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Bolcant dades de la taula `Pais`
--

INSERT INTO `Pais` (`idPais`, `pais`) VALUES
(1, 'Portugal'),
(2, 'Alemania'),
(3, 'Inglaterra'),
(4, 'Australia'),
(5, 'Italia'),
(6, 'EEUU');

-- --------------------------------------------------------

--
-- Estructura de la taula `Rol`
--

CREATE TABLE `Rol` (
  `idRol` int(11) NOT NULL,
  `rol` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Bolcant dades de la taula `Rol`
--

INSERT INTO `Rol` (`idRol`, `rol`) VALUES
(1, 'USER'),
(2, 'ADMIN');

-- --------------------------------------------------------

--
-- Estructura de la taula `Usuario`
--

CREATE TABLE `Usuario` (
  `idUsuario` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `apellidos` varchar(60) NOT NULL,
  `email` varchar(50) NOT NULL,
  `contrasena` varchar(10) NOT NULL,
  `fechaNacimiento` date DEFAULT NULL,
  `fechaRegistro` date NOT NULL,
  `idRol` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Bolcant dades de la taula `Usuario`
--

INSERT INTO `Usuario` (`idUsuario`, `nombre`, `apellidos`, `email`, `contrasena`, `fechaNacimiento`, `fechaRegistro`, `idRol`) VALUES
(1, 'nombre1', 'apellido1 apellido 2', 'correo1@correo.com', '1234', '1978-03-27', '2019-04-27', 1),
(2, 'nombre2', 'akjlsdkdj erjejrhhd', 'correo2@correo.com', '1234', '1982-10-09', '2019-04-10', 1);

-- --------------------------------------------------------

--
-- Estructura de la taula `UsuarioTienePreferencias`
--

CREATE TABLE `UsuarioTienePreferencias` (
  `idPreferencias` int(11) NOT NULL,
  `idUsuario` int(11) NOT NULL,
  `idCategoria` int(11) NOT NULL,
  `fechaCreacion` date NOT NULL,
  `idPais` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexos per taules bolcades
--

--
-- Index de la taula `Articulo`
--
ALTER TABLE `Articulo`
  ADD PRIMARY KEY (`idArticulo`),
  ADD KEY `fk_Juego_1_idx` (`idCategoria`),
  ADD KEY `fk_Articulo_1_idx` (`idPais`);

--
-- Index de la taula `Categoria`
--
ALTER TABLE `Categoria`
  ADD PRIMARY KEY (`idCategoria`);

--
-- Index de la taula `Pais`
--
ALTER TABLE `Pais`
  ADD PRIMARY KEY (`idPais`);

--
-- Index de la taula `Rol`
--
ALTER TABLE `Rol`
  ADD PRIMARY KEY (`idRol`);

--
-- Index de la taula `Usuario`
--
ALTER TABLE `Usuario`
  ADD PRIMARY KEY (`idUsuario`),
  ADD KEY `fk_Usuario_1_idx` (`idRol`);

--
-- Index de la taula `UsuarioTienePreferencias`
--
ALTER TABLE `UsuarioTienePreferencias`
  ADD PRIMARY KEY (`idPreferencias`),
  ADD KEY `fk_Comentario_1_idx` (`idUsuario`),
  ADD KEY `fk_UsuarioTienePreferencias_1_idx` (`idCategoria`);

--
-- AUTO_INCREMENT per les taules bolcades
--

--
-- AUTO_INCREMENT per la taula `Articulo`
--
ALTER TABLE `Articulo`
  MODIFY `idArticulo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT per la taula `Usuario`
--
ALTER TABLE `Usuario`
  MODIFY `idUsuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT per la taula `UsuarioTienePreferencias`
--
ALTER TABLE `UsuarioTienePreferencias`
  MODIFY `idPreferencias` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Restriccions per taules bolcades
--

--
-- Restriccions per la taula `Articulo`
--
ALTER TABLE `Articulo`
  ADD CONSTRAINT `fk_Articulo_1` FOREIGN KEY (`idPais`) REFERENCES `Pais` (`idPais`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Juego_1` FOREIGN KEY (`idCategoria`) REFERENCES `Categoria` (`idCategoria`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restriccions per la taula `Usuario`
--
ALTER TABLE `Usuario`
  ADD CONSTRAINT `fk_Usuario_1` FOREIGN KEY (`idRol`) REFERENCES `Rol` (`idRol`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restriccions per la taula `UsuarioTienePreferencias`
--
ALTER TABLE `UsuarioTienePreferencias`
  ADD CONSTRAINT `fk_Comentario_1` FOREIGN KEY (`idUsuario`) REFERENCES `Usuario` (`idUsuario`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_UsuarioTienePreferencias_1` FOREIGN KEY (`idCategoria`) REFERENCES `Categoria` (`idCategoria`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
