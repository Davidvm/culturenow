<?php
defined('BASEPATH') or exit('No se permite acceso directo');

require_once ROOT . FOLDER_PATH .'/app/models/Register/RegisterModel.php';
require_once LIBS_ROUTE .'Session.php';

/**
* Login controller
*/
class RegisterController extends Controller {
    private $model;
    private $session;

    public function __construct() {
        $this->model = new RegisterModel();
        $this->session = new Session();
    }

    public function exec() {
        $this->render(__CLASS__);
    }

    public function registerUser($request_params) {

        if ($this->verify($request_params))
            return $this->renderErrorMessage('Todos los campos son obligatorios');

        $user = new Usuario();

        $user->nombre = $_REQUEST['nombre'];
        $user->apellidos = $_REQUEST['apellidos'];
        $user->email = $_REQUEST['email'];
        $user->contrasena = $_REQUEST['password'];
        $user->fechaNacimiento = $_REQUEST['fechaNacimiento'];

        $this->model->insertUser($user);

        header('location: /culturenow/login');
    }

    private function verify($request_params) {
        return empty($request_params['nombre']) OR empty($request_params['apellidos']) OR empty($request_params['email'])
            OR empty($request_params['password']) OR empty($request_params['repassword']) OR empty($request_params['fechaNacimiento']);
    }

    private function renderErrorMessage($message) {
        $params = array('error_message' => $message);
        $this->render(__CLASS__, $params);
    }
}