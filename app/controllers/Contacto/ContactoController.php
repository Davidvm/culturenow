<?php
defined('BASEPATH') or exit('No se permite acceso directo');
require_once ROOT . '/culturenow/app/models/Contacto/ContactModel.php';
/**
 * Contacto controller
 */
class ContactoController extends Controller {
  /**
   * string 
   */
  public $nombre;

  /**
   * object 
   */
  public $model;

  /**
   * Inicializa valores 
   */
  public function __construct()
  {
    $this->model = new ContactModel();
    $this->nombre = 'Mundo';
  }

  /**
  * Método estándar
  */
  public function exec()
  {
    $this->show();
  }

  /**
  * Método de ejemplo
  */
  public function show()
  {
    $params = array('nombre' => $this->nombre);
    $this->render(__CLASS__, $params); 
  }

}