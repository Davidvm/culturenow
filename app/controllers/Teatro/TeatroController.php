<?php
defined('BASEPATH') or exit('No se permite acceso directo');
/**
 * Teatro controller
 */
class TeatroController extends Controller {

  public $nombre;
  public $model;

  /**
   * Inicializa valores 
   */
  public function __construct()
  {
    $this->nombre = 'Mundo';
  }

  /**
  * Método estándar
  */
  public function exec()
  {
    $this->show();
  }

  /**
  * Método de ejemplo
  */
  public function show()
  {
    $params = array('nombre' => $this->nombre);
    $this->render(__CLASS__, $params); 
  }

}