<?php
defined('BASEPATH') or exit('No se permite acceso directo');

require_once ROOT . FOLDER_PATH . '/app/models/Rss.php';
require_once ROOT . FOLDER_PATH . '/app/models/Usuario.php';
require_once LIBS_ROUTE . 'Session.php';

/**
 * Main controller
 */
class MainController extends Controller
{
    private $session;
    private $user;

    public function __construct()
    {
        $this->model = new Rss();
        $this->session = new Session();
        $this->user = new Usuario();

        $this->session->init();
        if ($this->session->getStatus() === 1 || empty($this->session->get('email')))
            exit('Acceso denegado');
    }

    public function SaveNewRss($request_params)
    {
        $rss = new Rss();

        if (@simplexml_load_file($request_params['url'])) {
            $rss->setLink($_REQUEST['url']);
            $rss->titulo = $_REQUEST['titulo'];
            $rss->id_user = $this->session->get('id');
            $this->model->insertRss($rss);

        header('location: /culturenow/main');

        } else {
            return $this->renderErrorMessage('No es un RSS valido');
        }
    }

    public fun

    public function exec()
    {
        $params = array('rss' => $this->user->getRssUser($this->session->get('id')));
        $this->render(__CLASS__, $params);
    }

    public function logout()
    {
        $this->session->close();
        header('location: /culturenow/login');
    }

    private function renderErrorMessage($message)
    {
        $params = array('error_message' => $message);
        $this->render(__CLASS__, $params);
    }

}
