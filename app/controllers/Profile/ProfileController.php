<?php
defined('BASEPATH') or exit('No se permite acceso directo');
require_once ROOT . FOLDER_PATH . '/app/models/Usuario.php';
require_once LIBS_ROUTE . 'Session.php';

/**
 * Cine controller
 */
class ProfileController extends Controller {
    private $user;
    private $session;

    /**
     * Inicializa valores
     */
    function __construct() {
        $this->user = new Usuario();
        $this->session = new Session();

        $this->session->init();
        if ($this->session->getStatus() === 1 || empty($this->session->get('email')))
            exit('Acceso denegado');
    }

    public function updateUser($request_params) {

        if ($this->verify($request_params))
            return $this->renderErrorMessage('Todos los campos son obligatorios');

        $user = new Usuario();

        $user->nombre = $_REQUEST['nombre'];
        $user->apellidos = $_REQUEST['apellidos'];
        $user->email = $_REQUEST['email'];
        $user->contrasena = $_REQUEST['password'];
        $user->idUsuario = $this->session->get('id');
        $user->fechaNacimiento = $_REQUEST['fechaNacimiento'];

        $this->user->updateUser($user);

        header('location: /culturenow/profile');
    }

    private function verify($request_params) {
        return empty($request_params['nombre']) OR empty($request_params['apellidos']) OR empty($request_params['email'])
            OR empty($request_params['password']) OR empty($request_params['repassword']) OR empty($request_params['fechaNacimiento']);
    }

    private function renderErrorMessage($message) {
        $params = array('error_message' => $message);
        $this->render(__CLASS__, $params);
    }


    /**
     * Método estándar
     */
    public function exec() {
        $this->show();
    }

    /**
     * Método de ejemplo
     */
    public function show() {
        $params = array('user' => $this->user->getProfileUser($this->session->get('id')));
        $this->render(__CLASS__, $params);
    }

}