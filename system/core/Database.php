<?php
defined('BASEPATH') or exit('No se permite acceso directo');

/**
* Clase de la Base de datos y conexión
*/
class Database {
	/**
	* @var $_dbUser
	*/
	private $_dbUser;

	/**
	* @var $_dbPassword
	*/
	private $_dbPassword;

	/**
	* @var $_dbHost
	*/
	private $_dbHost;

	/**
	* @var $_dbName
	*/
	protected $_dbName;

	/**
	* @var $_connection
	*/
	public $_connection;

    /**
    * @var $_instance
    */
    private static $_instance;


  	public function __construct() {
        try {
            $this->_dbHost = HOST;
            $this->_dbUser = USER;
            $this->_dbPassword = PASSWORD;
            $this->_dbName = DB_NAME;

            $this->_connection = new \PDO('mysql:host='.$this->_dbHost.'; dbname='.$this->_dbName, $this->_dbUser, $this->_dbPassword);
            $this->_connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $this->_connection->exec("SET CHARACTER SET utf8");
        } catch (\PDOException $e) {
            print "Error!: " . $e->getMessage();
            die();
        }
	}

    public function prepare($sql) {
        return $this->_connection->prepare($sql);
    }

    public static function instance() {
        if (!isset(self::$_instance))
        {
            $class = __CLASS__;
            self::$_instance = new $class;
        }
        return self::$_instance;
    }
}