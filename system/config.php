<?php
defined('BASEPATH') or exit('No se permite acceso directo');

// Valores de uri
define('URI', $_SERVER['REQUEST_URI']);
define('REQUEST_METHOD', $_SERVER['REQUEST_METHOD']);

// Valores de rutas
define('FOLDER_PATH', '/culturenow');
define('ROOT', $_SERVER['DOCUMENT_ROOT']);
define('PATH_VIEWS', FOLDER_PATH . '/app/views/');
define('PATH_CONTROLLERS', 'app/controllers/');
define('HELPER_PATH', 'system/helpers/');
define('ASSETS', ROOT . FOLDER_PATH . "app/assets/");
define('LIBS_ROUTE', ROOT . FOLDER_PATH . '/system/libs/');

// Valores de core
define('CORE', 'system/core/');
define('DEFAULT_CONTROLLER', 'Home');

// Valores de base de datos
define('HOST', 'localhost');
define('USER', 'root');
define('PASSWORD', 'root');
define('DB_NAME', 'allyournews');

// Valores configuracion
define('ERROR_REPORTING_LEVEL', -1);